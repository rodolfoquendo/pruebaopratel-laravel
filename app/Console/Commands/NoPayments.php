<?php

namespace App\Console\Commands;
use App\Payment;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;
use App\General;
class NoPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:NoPayments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    public static function checkIfUserPayedToday($user,$payments){
	$return=0;
	foreach($payments as $payment){
	    if($payment['phone']==$user['phone']){
		$return++;
	    }
	}
	return $return;
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	$users=User::select('phone')->get()->toArray();
	$payments=Payment::select('phone')
		->whereDate('created_at', date('Y-m-d'))
		->distinct('phone')
		->get()->toArray();
	if(count($payments)==0){
	    $payments=[];
	    $cont=count($users);
	}else{
	    $cont=0;
	    foreach($users as $user){
		$cont+= self::checkIfUserPayedToday($user,$payments);
	    }
	}
	$mail=General::sendMail('emails.NoPaymentscommand',['cantidad'=>$cont]);
	if(isset($mail['errors'])){
	    die(json_encode($mail));
	}
    }
}
