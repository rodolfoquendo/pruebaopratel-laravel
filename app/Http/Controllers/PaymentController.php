<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Payment;
class PaymentController extends Controller
{
    public function details(Request $request){
	Session::setId($request->ssid);
	Session::start();
	if(empty(Auth::user()->id)){
	    return ['errors'=>'nossesion'];
	}
	if(empty($request->id) || $request->id==''){
	    return ['errors'=>'ID Invalido'];
	}
	$id=$request->id;
	$payment=Payment::select()->where('id','=',$id)->get();
	return json_encode($payment[0]);
    }
    public function save(Request $request){
	Session::setId($request->ssid);
	Session::start();
	if(isset($request->id)){
	    $id=$request->id;
	    if(Auth::user()->type!='admin'){
		return json_encode([
		    'success'=>false,
		    'errors'=>'No tienes permisos.'
		]);
	    }
	    $payment=Payment::find($id);
	}else{
	    $payment=new Payment();
	}
	$data=[
	    'phone'=>$request->phone,
	    'amount'=>$request->amount,
	];
	if($data['phone']=='' || $data['phone']==null){
	    $data['phone']=Auth::user()->phone;
	}
	$store=\App\General::dbStorage($payment,$data);
	return json_encode([
	    'success'=>$store['success'],
	    'errors'=>$store['errors']
	]);
    }
    public function lista(Request $request){
	Session::setId($request->ssid);
	Session::start();
	if(Auth::user()->type!='admin'){
	    return json_encode(Payment::select()->where('phone','=',Auth::user()->phone)->get());
	}else{
	    return json_encode(Payment::all());
	}
    }
}
