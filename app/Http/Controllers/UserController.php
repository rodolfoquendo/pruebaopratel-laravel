<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
class UserController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {
        $credentials = ['email'=>$request->user,'password'=>$request->pass];
	$return=[];
        if (Auth::attempt($credentials,true)) {
	    $user=Auth::user();
            $return['success']=true;
	    $return['messages']['message']=trans('success.login.sucess');
	    $return['messages']['user']=$user->name;
	    $return['messages']['type']=$user->type;
	    $return['messages']['sessionid']=Session::getId();
	}else{
	    $return['success']=false;
	    $return['errors'][]=trans('errors.login.fail');
	}
	return json_encode($return);
    }
    public function logout(Request $request){
	$return=[];
	if(isset(Auth::user()->id)){
	    Auth::logout();
	}else{
	    $return['errors'][]='nosession';
	}
	$return['success']=true;
	return json_encode($return);
    }
    public function details(Request $request){
	Session::setId($request->ssid);
	Session::start();
	if(empty(Auth::user()->id)){
	    return ['errors'=>'nossesion'];
	}
	$id=($request->id==null)?Auth::user()->id:$request->id;
	$user=User::select()->where('id','=',$id)->get();
	return json_encode($user[0]);
    }
    public function save(Request $request){
	Session::setId($request->ssid);
	Session::start();
	if(isset($request->id)){
	    $id=$request->id;
	    if($id!=Auth::user()->id && Auth::user()->type!='admin'){
		return json_encode([
		    'success'=>false,
		    'errors'=>'No puedes modificar un usuario ajeno.'
		]);
	    }
	    $user=User::find($id);
	}else{
	    $user=new User();
	}
	$data=[
	    'phone'=>$request->phone,
	    'email'=>$request->email,
	    'name'=>$request->name
	];
	if(isset($request->password) && $request->password!=null){
	    $data['password']=\Illuminate\Support\Facades\Hash::make($request->password);
	}
	$store=\App\General::dbStorage($user,$data);
	return json_encode([
	    'success'=>$store['success'],
	    'errors'=>$store['errors']
	]);
    }
    public function lista(Request $request){
	Session::setId($request->ssid);
	Session::start();
	if(Auth::user()->type!='admin'){
	    return json_encode([
		'success'=>false,
		'errors'=>'No puedes modificar un usuario ajeno.'
	    ]);
	}else{
	    return json_encode(User::all());
	}
    }
}