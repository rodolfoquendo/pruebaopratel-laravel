<?php
namespace App;
use Illuminate\Support\Facades\Mail;
/**
 * General functions to be used through all the classes
 *
 * @author Rodolfo
 */
class General{
    public static function sendMail($template,$vars,$cont=0){
        $error=null;
        try{
            Mail::send($template, $vars, function ($message){
                $message->from(
                    'rodolfoquendo@outlook.com',
                    'Prueba Opratel'
                );
                $message->subject('Reporte del dia');
                $message->to('rodolfoquendo@gmail.com');
            });
        }catch(Exception $e){
            $error=$e;
            $cont++;
        }
        if($cont>0 && $cont<3){
            self::sendMail($template,$user,$vars,$cont);
        }else{
            return ['errors'=>$error];
        }
    }
    public static function dbStorage($object,$dataarray){
        $return=[
            'errors'=>[],
            'success'=>false,
            'object'=>''
        ];
        try{
            $return['object']= \Illuminate\Support\Facades\DB::transaction(function() use($object,$dataarray){
                foreach($dataarray as $property=>$value){
		    $object->{$property}=$value;
		}
		$object->save();
		return $object;
            },5);
        }catch(Exception $e){
            $return['errors'][]=$e;
        }
        $return['success']=(count($return['errors'])>0)?false:true;
        return $return;
    }
}