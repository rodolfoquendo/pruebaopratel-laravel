<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/time',function(){
    if(isset($_GET['ssid'])){
	Session::setId($_GET['ssid']);Session::start();
    }
    return json_encode([
        'time'=>round(microtime(true)*1000),
        'datetime-withseconds'=>date('Y-m-d H:i:s'),
        'datetime-withoutseconds'=>date('Y-m-d H:i'),
        'datetime-plain'=>date('YmdHis')
    ]);
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login','UserController@login');
Route::get('/logout','UserController@logout');
Route::get('/user','UserController@details');
Route::get('/user/list','UserController@lista');
Route::post('user/save', [
    'as' => 'user.save', 'uses' => 'UserController@Save',
]);
Route::get('/payment','PaymentController@details');
Route::get('/payment/list','PaymentController@lista');
Route::post('payment/save', [
    'as' => 'payment.save', 'uses' => 'PaymentController@Save',
]);
