Esta es la prueba para Opratel<br>

La configuracion seria la siguiente:<br>
<ul>
<li>Dominio (agregado en hosts y en el localhost puerto 80 hacia path/to/localhost/pruebaopratel-laravel/public)
<ul><li>http://opratel.local para la api en laravel</li><li>http://opratel.view para el html que consume</li></ul></li>
<li>DB tal como sale en el .env</li>
<li>Commando: php artisan command:NoPayments (configurar en el env las opciones del smtp)</li>
<li>Para el proposito de la prueba y el parametro tiempo, no se esta considerando nada de seguridad, ni encriptado, ni seguridad de las peticiones, ni seguridad de las cookies, etc.</li>
</ul>


BD


CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `amount` decimal(17,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `payments` (`id`, `phone`, `amount`, `created_at`, `updated_at`) VALUES
(1, '+541127416098', '100.00', '2019-06-24 02:50:33', '2019-06-25 02:11:56'),
(2, '+541127416098', '15.00', '2019-06-24 02:57:58', '2019-06-24 02:57:58'),
(3, '4149589922', '123.00', '2019-06-25 02:20:06', '2019-06-25 02:20:06');
CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('cliente','admin') CHARACTER SET utf8mb4 DEFAULT 'cliente',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
INSERT INTO `users` (`id`, `name`, `email`, `phone`, `email_verified_at`, `password`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Rodolfo', 'rodolfoquendo@gmail.com', '+541127416098', '2019-06-23 12:00:00', '$2y$10$k2Ct9rFZ1jFJAEGc4m98/.H6DPBU0PuwEo6AqQdWQ.IoHCG5oFewi', 'admin', 'W9D6xNAYgNun0cAEfPZAujartNLGoogqpfGP4sHVpIEaUX2bp0BgHW81Aowy', '2019-06-23 03:52:47', '2019-06-24 01:04:00'),
(5, 'Rodolfo A Oquendo R', 'rodolfoquendo@outlook.com', '4149589922', NULL, '$2y$10$KWObgdk4v160WGePg/38Dug3G5UtuO.PCGJgzaZEUVGzrYiethMYO', 'cliente', 'EjHgwJg6iY8kMr0OeRLoCqkW9X3dEKqIgGpjuW1Bshf8lFjr8llNBKKONEve', '2019-06-24 01:47:51', '2019-06-24 01:47:51'),
(6, 'Rodolfo A Oquendo R', 'rodolfoquendo@outlook.cos', '4149589922', NULL, '$2y$10$SoxvySIZMyD92A6bm1Jf9OnIYZTMMUzk0H96lceTtfMAYFXa.7nUW', 'cliente', NULL, '2019-06-24 01:48:16', '2019-06-24 01:48:16'),
(7, 'Rodolfo A Oquendo R', 'rodolfoquendo@outlook.coms', '4149589922', NULL, '$2y$10$qDwXUpSlpb..y/uDohxeN.Y/Sh6mRLOq4ihLuPnTYboIDaIz8etK.', 'cliente', NULL, '2019-06-24 01:49:39', '2019-06-24 01:49:39'),
(8, 'asdasd', 'rodolfoquendo@outlook.comdsadasd', 'asdasdas', NULL, '$2y$10$9ppPVNDYPIVXeJGQNgUj/eqB2.aAuQL1gDd0mkRobapXSNfUOEZYS', 'cliente', NULL, '2019-06-24 01:51:25', '2019-06-24 01:51:25'),
(9, 'asd', 'rodolfoquendo@outlook.comasdasda', 'sdasdasd', NULL, '$2y$10$aiCK22.9WGjj2Xco7XmDv.68DOY6Tj5gLdSQZtuKVLDJuW2WtUS.S', 'cliente', NULL, '2019-06-24 01:53:01', '2019-06-24 01:53:01'),
(10, 'asd', 'rodolfoquendo@outlook.comasdasda', 'sdasdasd', NULL, '$2y$10$g6Hl1a.5HVDRLYevxyWhLusJ2EFaRtHN9lqIuUAAHtBKIH1FYSk5O', 'cliente', NULL, '2019-06-24 01:53:43', '2019-06-24 01:53:43'),
(11, 'asdada', 'rodolfoquendo@outlook.comasdasdas', 'dasdasd', NULL, '$2y$10$ChyqH/eE7FjIzfJRLj.F2ur3iYPLvcSuemnS1h1xP0RqDXAQTdpfi', 'cliente', NULL, '2019-06-24 01:53:52', '2019-06-24 01:53:52'),
(12, '123456', 'rodolfoquendo@gmail.comasdd', 'dsasda', NULL, '$2y$10$o607vhPjgCXCBZK9MrsHKeQFuEw8CzaFeZSMm2vfJHtyenjeJIOmG', 'cliente', NULL, '2019-06-24 01:56:22', '2019-06-24 01:56:22'),
(13, '231weq', 'rodolfoquendo@gmail.comdsada', 'asda', NULL, '$2y$10$PFTKP9H3NlSgNCAhhSxZJ.SdN1dThcdpNuuaOYmplVP34PMr1jOdG', 'cliente', NULL, '2019-06-24 01:56:53', '2019-06-24 01:56:53'),
(14, '12312313easddas', 'rodolfoquendo@gmail.comasda', '123esa', NULL, '$2y$10$5EmKxLQOahaTYCkBGjyTquCldoINtTIF7jNcn5EtsH86rPW.P.oS.', 'cliente', 'GjAh551sZnzPJ2ClTPD05HdsSXM6Ci6ByulhQMU425aScWLzVlZtUheMXHgz', '2019-06-24 01:58:11', '2019-06-24 01:58:11');

ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;COMMIT;
